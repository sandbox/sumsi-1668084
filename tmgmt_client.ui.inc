<?php

/**
 * Default remote translation plugin controller.
 */
class TMGMTDefaultRemoteTranslatorUIController extends TMGMTDefaultTranslatorUIController {

  /**
   * Overrides TMGMTDefaultTranslatorUIController::pluginSettingsForm().
   */
  public function pluginSettingsForm($form, &$form_state, TMGMTTranslator $translator, $busy = FALSE) {
    $form['url'] = array(
      '#type' => 'textfield',
      '#title' => t('Server URL'),
      '#default_value' => $translator->getSetting('url'),
      '#description' => t('Please enter the URL of the translation server.'),
      '#element_validate' => array('tmgmt_client_element_validate_url'),
      '#required' => TRUE,
      '#disabled' => $busy,
    );

    return $form;
  }

}

/**
 * Default remote translation plugin controller.
 */
class TMGMTDrupalTranslatorUIController extends TMGMTDefaultRemoteTranslatorUIController {

  /**
   * Overrides TMGMTDefaultTranslatorUIController::pluginSettingsForm().
   */
  public function pluginSettingsForm($form, &$form_state, TMGMTTranslator $translator, $busy = FALSE) {
    $form = parent::pluginSettingsForm($form, $form_state, $translator, $busy);

    $form['public_key'] = array(
      '#type' => 'textfield',
      '#title' => t('Public key'),
      '#default_value' => $translator->getSetting('public_key'),
      '#description' => t('Please enter your public key.'),
      '#element_validate' => array('tmgmt_client_element_validate_api_key'),
      '#required' => TRUE,
      '#disabled' => $busy,
    );

    $form['private_key'] = array(
      '#type' => 'textfield',
      '#title' => t('Private key'),
      '#default_value' => $translator->getSetting('private_key'),
      '#description' => t('Please enter your private key.'),
      '#element_validate' => array('tmgmt_client_element_validate_api_key'),
      '#required' => TRUE,
      '#disabled' => $busy,
    );

    return $form;
  }

}