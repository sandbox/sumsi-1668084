<?php

/**
 * Default (base) implementation for translator plugins that connect with remote
 * translation services.
 */
class TMGMTDefaultRemoteTranslatorPluginController extends TMGMTDefaultTranslatorPluginController {

  /**
   * Overrides TMGMTPluginBase::__construct().
   */
  public function __construct($type, $plugin) {
    parent::__construct($type, $plugin);

    // By default all requests are targeted at the client itself.
    $this->serviceUrl = '';
  }

  /**
   * Implements TMGMTTranslatorPluginControllerInterface::requestTranslation().
   */
  public function requestTranslation(TMGMTJob $job) {
    // Build the request data array.
    $request = $this->getTranslationRequestArray($job);

    // Send the translation request to the server.
    $response = $this->post($job->getTranslator(), 'job', 'order', array('data' => json_encode($request)));

    if (!empty($response) && $response->code == 200) {
      // Decode the response array.
      $data = json_decode($response->data, TRUE);

      if (isset($data['reference'])) {
        $job->reference = $data['reference'];
      }

        // The job has been submitted successfully.
      $job->submitted();

      if (!empty($data['data']) && is_array($data['data'])) {
        // Process the response from the translation service.
        foreach ($data['data'] as $key => $item) {
          if (is_array($item)) {
            // In some cases the translation might already be finished.
            $this->processTranslatedData(tmgmt_job_item_load($key), $item);
          }
        }
      }
    }
    elseif (!empty($response)) {
      // Mark the translation job as 'rejected' in case of an error, whatever
      // that error might be.
      $job->rejected('The server responded with code @code: @message', array('@code' => $response->code, '@message' => $response->data));
    }
    else {
      // The server could not be reached.
      $job->rejected('Unable to reach the remote translation service while trying to send a translation request.');
    }
  }

  /**
   * Overrides TMGMTDefaultTranslatorPluginController::cancelTranslation().
   */
  public function cancelTranslation(TMGMTJob $job) {
    // We can't cancel a job that we don't know the remote reference of.
    if (!isset($job->reference)) {
      return FALSE;
    }

    // Try to cancel the translation job on the server.
    $response = $this->post($job->getTranslator(), 'job', 'cancel', array('id' => $job->reference));

    if (!empty($response) && $response->code == 200) {
      // We successfully cancelled the translation job.
      $job->cancelled();
      return TRUE;
    }
    if (!empty($response)) {
      // We encountered an error.
      // @todo Or the server said 'no'.
      watchdog('tmgmt', 'Could not cancel the translation job. The server returned error code @code: @error', array('@code' => $response->code, '@error' => $response->error), WATCHDOG_ERROR);
      return FALSE;
    }

    // The server could not be reached.
    watchdog('tmgmt', 'Unable to reach the remote translation service while trying to cancel the translation job.');
    return FALSE;
  }

  /**
   * Saves translated data in a job item.
   */
  public function processTranslatedData($item, $data) {
    $translation = array();
    foreach (tmgmt_flatten_data($data) as $path => $value) {
      if (isset($value['#translation']['#text'])) {
        $translation[$path]['#text'] = $value['#translation']['#text'];
      }
    }

    $item->addTranslatedData(tmgmt_unflatten_data($translation));
  }

  /**
   * Overrides TMGMTDefaultTranslatorPluginController::getSupportedTargetLanguages().
   */
  public function getSupportedTargetLanguages(TMGMTTranslator $translator, $language) {
    $request = array('language' => $language);
    $response = $this->get($translator, 'languages', '', $request);

    if (!empty($response) && $response->code == 200) {
      // We successfully retrieven the supported languages.
      return json_decode($response->data, TRUE);
    }

    if (!empty($response)) {
      // We encountered an error.
      watchdog('tmgmt', 'Could not retrieve supported target languages from translation server. The server returned error code @code: @error', array('@code' => $response->code, '@error' => $response->error), WATCHDOG_ERROR);
      return array();
    }

    // The server could not be reached.
    watchdog('tmgmt', 'Unable to reach the remote translation service while trying to fetch the supported languages information.');
    return array();
  }

  /**
   * Prepares a request data array for a translation request.
   *
   * @param $job
   *   The job for which the request data should be generated.
   *
   * @return array
   *   A fully prepared request array to be used in a translation request.
   */
  public function getTranslationRequestArray(TMGMTJob $job) {
    // Each translation service expects its own, unique request data structure.
    return array();
  }

  /**
   * Returns an array of the required authentication query arguments.
   *
   * @param $translator
   *   The translator entity.
   *
   * @return array
   *   The authentication information.
   */
  public function getAuthenticationQuery(TMGMTTranslator $translator) {
    // We can't provide any default authentication information. This is
    // specific to every single translation service.
    return array();
  }

  /**
   * Returns the URL for a given method.
   *
   * @param $translator
   *   The translator entity.
   * @param $method
   *   The method to call.
   *
   * @return string
   *   The absolute URL to call.
   */
  public function getUrl(TMGMTTranslator $translator, $resource, $method = '') {
    return $this->getServiceUrl($translator) . '/' . $resource . ($method ? '/' . $method : '');
  }

  /**
   * Returns the URL of the service.
   *
   * @param $translator
   *   The translator entity.
   *
   * @return string
   *   The absolute URL of the service.
   */
  public function getServiceUrl(TMGMTTranslator $translator) {
    return $this->serviceUrl;
  }

  /**
   * Executes a get request.
   *
   * @param $translator
   *   Translator object.
   * @param $method
   *   The REST method to call.
   * @param $query
   *   Arguments for the method.
   *
   * @return stdClass
   *   The service response.
   */
  protected function get(TMGMTTranslator $translator, $resource, $method = '', $query = array(), $authenticate = TRUE) {
    // Add the authentication information unless we don't want that.
    $query = $authenticate ? array_merge($query, $this->getAuthenticationQuery($translator)) : $query;
    $url = url($this->getUrl($translator, $resource, $method), array('query' => $query, 'absolute' => TRUE));

    return drupal_http_request($url);
  }

  /**
   * Executes a post request.
   *
   * @param $translator
   *   The translator entity.
   * @param $method
   *   The method to call.
   * @param $query
   *   The POST data to submit.
   *
   * @return stdClass
   *   The service response.
   */
  protected function post(TMGMTTranslator $translator, $resource, $method, $query = array(), $authenticate = TRUE) {
    // Add the authentication information unless we don't want that.
    $query = $authenticate ? array_merge($query, $this->getAuthenticationQuery($translator)) : $query;
    $url = url($this->getUrl($translator, $resource, $method));

    $options = array(
      'headers' => array(
        'Content-type' => 'application/x-www-form-urlencoded',
      ),
      'method' => 'POST',
      'data' => drupal_http_build_query($query),
    );

    return drupal_http_request($url, $options);
  }

  /**
   * Executes a put request.
   *
   * @param $translator
   *   The translator entity.
   * @param $method
   *   The method to call.
   * @param $query
   *   The PUT data to submit.
   *
   * @return stdClass
   *   The service response.
   */
  protected function put(TMGMTTranslator $translator, $resource, $method, $query = array(), $authenticate = TRUE) {
    // Add the authentication information unless we don't want that.
    $query = $authenticate ? array_merge($query, $this->getAuthenticationQuery($translator)) : $query;
    $url = url($this->getUrl($translator, $resource, $method), array('query' => $query));

    $options = array(
      'headers' => array(
        'Content-type' => 'application/json',
      ),
      'method' => 'PUT',
    );

    return drupal_http_request($url, $options);
  }

}

/**
 * Translator plugin controller class for the client translator plugin.
 */
class TMGMTDrupalTranslatorPluginController extends TMGMTDefaultRemoteTranslatorPluginController {

  /**
   * Overrides TMGMTDefaultTranslatorPluginController::isAvailable().
   */
  public function isAvailable(TMGMTTranslator $translator) {
    if ($translator->getSetting('url') && $translator->getSetting('public_key') && $translator->getSetting('private_key')) {
      return TRUE;
    }

    return FALSE;
  }

  /**
   * Overrides
   * TMGMTDefaultRemoteTranslatorPluginController::getAuthenticationQuery().
   */
  public function getAuthenticationQuery(TMGMTTranslator $translator) {
    $timestamp = microtime(TRUE);

    return array(
      'public_key' => $translator->getSetting('public_key'),
      'signed_key' => hash_hmac('sha256', $timestamp, $translator->getSetting('private_key')),
      'timestamp' => $timestamp,
    );
  }

  /**
   * Overrides
   * TMGMTDefaultRemoteTranslatorPluginController::getTranslationRequestArray().
   */
  public function getTranslationRequestArray(TMGMTJob $job) {
    $translations = array(
      'items' => array(),
      'from' => $job->source_language,
      'to' => $job->target_language,
    );

    foreach ($job->getItems() as $item) {
      $translations['items'][$item->tjiid] = $this->getTranslationRequestItemArray($item);
    }

    return $translations;
  }

  /**
   * Overrides TMGMTDefaultRemoteTranslatorPluginController::getServiceUrl().
   */
  public function getServiceUrl(TMGMTTranslator $translator) {
    return $translator->getSetting('url');
  }

  /**
   * Builds the translation request array for a job item in a translation
   * request array.
   *
   * @param $item
   *   The job item to generate the request array for.
   *
   * @return array
   *   An array representing the translation request data for the passed job
   *   item.
   */
  public function getTranslationRequestItemArray(TMGMTJobItem $item) {
    $data = array(
      'data' => $this->getTranslationRequestItemData($item),
      'label' => $this->getTranslationRequestItemLabel($item),
      'callback' => $this->getCallbackUrl($item),
    );

    return $data;
  }

  /**
   * Retrieves the filtered and structured data array for a single job item in
   * a translation request array.
   *
   * @param $item
   *   The job item to retrieve the structured data array for.
   *
   * @return array
   *   The structured data array for the passed job item.
   */
  public function getTranslationRequestItemData(TMGMTJobItem $item) {
    $filtered = array_filter(tmgmt_flatten_data($item->getData()), '_tmgmt_filter_data');

    // Return a filtered, unflattened data structure.
    return tmgmt_unflatten_data($filtered);
  }

  /**
   * Retrieves the label for a job item in a translation request array.
   *
   * @param $item
   *   The job item to retrieve the label for.
   *
   * @return string
   *   The label of the job item.
   */
  public function getTranslationRequestItemLabel(TMGMTJobItem $item) {
    return $item->getSourceLabel();
  }

  /**
   * Retrieve the callback url for a job item.
   */
  public function getCallbackUrl(TMGMTJobItem $item) {
    return url('tmgmt-' . str_replace('_', '-', $this->pluginType) . '-callback/' . $item->tjiid, array('absolute' => TRUE));
  }

  /**
   * Retrieve the url of the remote translation service.
   */
  public function getUrl(TMGMTTranslator $translator, $resource, $method = '') {
    return $this->getServiceUrl($translator) . '/' . str_replace('_', '-', $this->pluginType) . '-translator-' . $resource . ($method ? '/' . $method : '');
  }

  /**
   * Pulls a translation from the server
   */
  public function pullTranslation($translator, $item, $id) {
    $response = $this->get($translator, 'item', $id);

    if (!empty($response) && $response->code == 200) {
      $this->processTranslatedData($item, json_decode($response->data, TRUE));
    }
    elseif (!empty($response)) {
      watchdog('tmgmt', 'Could not pull translation data from translation server. The server returned error code @code: @error', array('@code' => $response->code, '@error' => $response->error), WATCHDOG_ERROR);
    }
    else {
      watchdog('tmgmt', 'Unable to reach the remote translation service while trying to send a pull request.');
    }
  }
}